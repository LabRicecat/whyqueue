CFLAGS=-O2 -shared
LIBNAME=libwhyqueue
LIBINSTALL=/usr/lib/
INCLUDEINSTALL=/usr/include/

whyqueue: whyqueue.c whyqueue.h
	$(CC) $(CFLAGS) whyqueue.c -o $(LIBNAME).so

install: whyqueue
	cp $(LIBNAME).so $(LIBINSTALL)
	cp whystack.h $(INCLUDEINSTALL)

