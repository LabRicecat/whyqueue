# whyqueue

A queue built with unix pipes.  
Why? Exactly.

## Installation
```
$ make 
$ make install
```

## Functions
```
void     whypush    (whyqueue*, size_t, void*)  pushes an element
void*    whypop     (whyqueue*, size_t)         pops an element 
whyqueue whyopen    ()                          creates a new queue
void     whyclose   (whyqueue*)                 destroys a queue 
int      whygood    (whyqueue*)                 checks if a queue is good 
void     whyreopen  (whyqueue*)                 reopens a queue 
int      whylen     (whyqueue*)                 returns the length of a queue 
void     whyclear   (whyqueue*)                 clears all elements in a queue
```
