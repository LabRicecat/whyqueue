#include "whystack.h"
#include <stdlib.h>
#include <unistd.h>

void whypush(struct whyqueue* ws, size_t s, void* c) {
    write(ws->put, c, s);
    ws->len += s;
}

void* whypop(struct whyqueue* ws, size_t s) {
    char* bf = malloc(s);
    read(ws->pin, bf , s);
    ws->len -= s;

    return bf;
}

struct whyqueue whyopen(void) {
    struct whyqueue ws;
    int p[2];
    pipe(p);

    ws.len = 0;
    ws.pin = p[0];
    ws.put = p[1];

    return ws;
}

void whyclose(struct whyqueue* ws) {
    close(ws->pin);
    close(ws->put);
    ws->len = 
    ws->pin = 
    ws->put = -1;
}

int whygood(struct whyqueue* ws) {
    return ws != NULL && ws->len != -1 && ws->pin != -1 && ws->put != -1;
}

void whyreopen(struct whyqueue* ws) {
    if(whygood(ws))
        whyclose(ws);

    *ws = whyopen();
}


int whylen(struct whyqueue* ws) {
    return ws->len;
}

void whyclear(struct whyqueue* ws) {
    free(whypop(ws,whylen(ws)));
}
