#ifndef WHYQUEUE_H
#define WHYQUEUE_H

#include <stddef.h>

struct whyqueue {
    int pin;
    int put;
    int len;
};

void whypush(struct whyqueue* ws, size_t s, void* c);

void* whypop(struct whyqueue* ws, size_t s);

struct whyqueue whyopen(void);

void whyclose(struct whyqueue* ws);

int whygood(struct whyqueue* ws);

void whyreopen(struct whyqueue* ws);

int whylen(struct whyqueue* ws);

void whyclear(struct whyqueue* ws);

#endif
